<html>
<header>
    <script type="text/javascript" charset="utf8" src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-1.8.2.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/r/dt/dt-1.10.9/datatables.min.css"/>
    <script type="text/javascript" src="https://cdn.datatables.net/r/dt/dt-1.10.9/datatables.min.js"></script>
    <script src="http://malsup.github.io/jquery.blockUI.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <style>
        #listLead {
            float: left;
        }
    </style>
</header>
<body>
<h3> BOT (EXPERIMENTAL) COBRANÇA BRADESCO </h3>
<div class="">
    <div class="form-group col-md-3">
        <label>Data Vencimento Inicial</label>
        <input type="text" class="form-control col-md-3" id="vencimento_inicial" maxlength="10">
    </div>
    <div class="form-group col-md-3">
        <label>Data Vencimento Final</label>
        <input type="text" class="form-control col-md-3" id="vencimento_final" maxlength="10">
    </div>
    <div style="clear: both;"></div>
    <div class="form-group col-md-3">
        <label>Mensagem</label>
        <textarea id="txtMessage" class="form-control" cols="20" rows="3">UNIMED ODONTO vence HOJE e o pagamento pode ser em qualquer LOTÉRICA </textarea>
    </div>
    <div class="form-group col-md-3">
        <button class="btn btn-primary" id="buscar_vencimentos" style="margin-top: 24px;">BUSCAR</button>
        <div class="run_bot"></div>
    </div><br>
    <div style="clear: both;"></div>
    <div class="container" id="listLead"></div>
</div>
<script>
    $(document).on('click','#run_boleto',function (e) {
        e.preventDefault();
        $("#listLead p").each(function(i,data){
            var idem = '#'+$(this).data('lead')+'_email';
            var idsm = '#'+$(this).data('lead')+'_sms';
            $(idem).click();
            $(idsm).click();
        });
    });
    function sleep(milliseconds) {
        var start = new Date().getTime();
        for (var i = 0; i < 1e7; i++) {
            if ((new Date().getTime() - start) > milliseconds){
                break;
            }
        }
    }
    //
    var generateBolDOM = function(data){
        for(i=0;i<data.length;i++){
            setTimeout(function(s) {
                return function() { autoGenerateBol(s) } }(data[i]), i*6000);
        }
    };

    var autoGenerateBol = function(this_){
        console.log(this_);
        var lead = this_;
        var promise = $.ajax({method:'get',url:'/gatewayab/generateBoletosVencimentos/'+lead.lead_vencimento_dia+'/'+lead.lead_id});
        promise.done(function (resp) {
            idbol = '#'+lead.lead_id+'_boleto';
            idem = '#'+lead.lead_id+'_email';
            idsm = '#'+lead.lead_id+'_sms';
            idlo = '#'+lead.lead_id+'_loading';
            idlep = '#lead_'+lead.lead_id;

            $(idlo).remove();

            $(idbol).removeAttr('style');
            $(idem).removeAttr('style');
            $(idsm).removeAttr('style');

            $(idbol).html('Boleto OK!');
            $(idbol).removeClass("btn-primary");
            $(idbol).removeClass("generate_bol");
            $(idbol).addClass("btn-success");

            $(idlep).append("<a class='btn btn-xs btn btn-primary' target='_blank' href='http://pag.agentebrasil.com/boletos/"+resp.cobranca+"_f.png'>Ver Boleto</a><br>");

            $(idbol).data("file",resp.file);
            $(idbol).data("linha_digitavel",resp.linha_digitavel);
            $(idbol).data("cobranca",resp.cobranca);
        });
        promise.fail(function (resp) {
            idlo = '#'+lead.lead_id+'_loading';
            idbol = '#'+lead.lead_id+'_boleto';
            $(idbol).removeClass("btn-primary");
            $(idbol).addClass("btn-danger");
            $(idbol).html("Erro ao Gerar o boleto");
            $(idbol).removeAttr('style');
            $(idlo).remove();
        });
    };
    //
    $(document).on('click','.generate_bol',function(){
        console.log($(this).data("lead"));
        var lead = $(this).data("lead");
        var promise = $.ajax({method:'get',url:'/gatewayab/generateBoletosVencimentos/'+lead.lead_vencimento_dia+'/'+lead.lead_id});
        promise.done(function (resp) {
            idbol = '#'+lead.lead_id+'_boleto';
            idem = '#'+lead.lead_id+'_email';
            idsm = '#'+lead.lead_id+'_sms';
            idlo = '#'+lead.lead_id+'_loading';
            idlep = '#lead_'+lead.lead_id;

            $(idlo).remove();

            $(idbol).removeAttr('style');
            $(idem).removeAttr('style');
            $(idsm).removeAttr('style');

            $(idlep).append("<a class='btn btn-xs btn btn-primary' target='_blank' href='http://pag.agentebrasil.com/boletos/"+resp.cobranca+"_f.png'>Ver Boleto</a><br>");
            $(idbol).html('Boleto OK!');
            $(idbol).removeClass("btn-primary");
            $(idbol).removeClass("generate_bol");
            $(idbol).addClass("btn-success");

            $(idbol).data("file",resp.file);
            $(idbol).data("linha_digitavel",resp.linha_digitavel);
            $(idbol).data("cobranca",resp.cobranca);

        });
        promise.fail(function (resp) {
            idbol = '#'+lead.lead_id+'_boleto';
            $(idbol).removeClass("btn-primary");
            $(idbol).addClass("btn-danger");
            $(idbol).html("Erro ao Gerar o boleto");
        });
    });
    //
    $(document).on('click','.send_email',function(){
        var lead = $(this).data("lead");
        var idbol = '#'+lead.lead_id+'_boleto';
        var idem = '#'+lead.lead_id+'_email';
        var idsm = '#'+lead.lead_id+'_sms';
        var promise = $.ajax({type:'post',dataType: 'json', url:'http://contact.agentebrasil.com/contact/legado/storeMailCobrancaBradesco',data:{
            'destinatario_email':lead.lead_email,
            'destinatario_nome':lead.lead_nome,
            'lead_id':lead.lead_id,
            'produto_nome_email':'Unimed Odonto',
            'remetente_email':"norepley@agentebrasil.com.br",
            'remetente_nome': "Agente Brasil",
            'cobranca':$(idbol).data('cobranca')
        }});
        promise.done(function(resp){
            $(idem).html('EMAIL IN QUEUE OK!');
            $(idem).removeClass("btn-primary");
            $(idem).removeClass("send_email");
            $(idem).addClass("btn-success");
        });
        promise.fail(function (resp) {
            $(idem).removeClass("btn-primary");
            $(idem).addClass("btn-danger");
            $(idem).html("EMAIL FAIL!");
        });
    });
    //
    $(document).on('click','.send_sms',function(){
        var lead = $(this).data("lead");
        var idbol = '#'+lead.lead_id+'_boleto';
        var idsm = '#'+lead.lead_id+'_sms';
        var promise = $.ajax({
            type:"POST",
            crossDomain: true,
            url:"http://contact.agentebrasil.com/contact/contact/store",
            dataType: 'json',
            data:{
                "canal_id": 2,
                "destinatario_fone":'55'+lead.lead_fone,
                "destinatario_nome":lead.lead_nome,
                "lead_id":lead.lead_id,
                "produto_nome_email":'Unimed Odonto',
                "remetente_nome": "Agente Brasil",
                "mensagem":$('#txtMessage').val() + $(idbol).data('file')+ ' linha digitavel ' + $(idbol).data('linha_digitavel')
            }
        });
        promise.done(function(resp){
            $(idsm).html('SMS IN QUEUE OK!');
            $(idsm).removeClass("btn-primary");
            $(idsm).removeClass("send_sms");
            $(idsm).addClass("btn-success");
        });
        promise.fail(function (resp) {
            $(idsm).removeClass("btn-primary");
            $(idsm).addClass("btn-danger");
            $(idsm).html("SMS FAIL!");
        });
    });
    //
    var listLead = function(data){
        html = '';
        for(i=0;i<data.length;i++){
            html+= "<p id='lead_"+data[i].lead_id+"' data-lead="+data[i].lead_id+">"+data[i].lead_id+' -- '+data[i].lead_nome+' -- '+data[i].lead_cpf+' -- '+data[i].lead_email+' -- '+
                    "<button class='btn btn-xs btn-warning' id='"+data[i].lead_id+"_loading'>Loading...</button>"+
                    "<button class='btn btn-xs btn btn-primary generate_bol' data-lead='"+JSON.stringify(data[i])+"' id='"+data[i].lead_id+"_boleto' style='display:none'>GERAR BOLETOS</button> | " +
                    "<button class='btn btn-xs btn-primary send_email' data-lead='"+JSON.stringify(data[i])+"' id='"+data[i].lead_id+"_email' style='display:none'>ENVIAR EMAIL</button> | " +
                    "<button class='btn btn-xs btn-primary send_sms' data-lead='"+JSON.stringify(data[i])+"' id='"+data[i].lead_id+"_sms' style='display:none'>ENVIAR SMS</button> | </p>";
        }
        return html;
    };
    //
    $('#buscar_vencimentos').on('click',function(){
        $.blockUI({message:'<img src="https://upload.wikimedia.org/wikipedia/commons/b/b1/Loading_icon.gif" width="90px" height="90px">'});
        var vencimento_inicial = $('#vencimento_inicial').val();
        var vencimento_final = $('#vencimento_final').val();
        var url = '/gatewayab/getLeadVencimento/'+vencimento_inicial;
        if(vencimento_final!==null){
            url+='/'+vencimento_final;
        }
        var promise = $.ajax({'method':'get','url':url});
        promise.done(function(resp){
            $('#listLead').html(listLead(resp));
            generateBolDOM(resp);
            $.unblockUI();
            $('.run_bot').html('<button class="btn btn-success" id="run_boleto" style="margin-top: 24px;">RUN BOT</button>');
        });
        promise.fail(function(){
            $.unblockUI();
        });
    });
    //
    $('#vencimento_inicial').on('keyup',function(e){
        if(e.which !== 8) {
            if($(this).val().length == 4){
                $('#vencimento_inicial').val($(this).val()+'-');
            } else if($(this).val().length == 7) {
                $('#vencimento_inicial').val($(this).val()+'-');
            }
        }
    });

    $('#vencimento_final').on('keyup',function(e){
        if(e.which !== 8) {
            if($(this).val().length == 4){
                $('#vencimento_final').val($(this).val()+'-');
            } else if($(this).val().length == 7) {
                $('#vencimento_final').val($(this).val()+'-');
            }
        }
    });
    //
    var separaLeads = function(){
        var leads = {};
        if($('#leads').val().indexOf(',')==-1){
            leads = $('#leads').val().split('\n');
        } else {
            leads = $('#leads').val().split(',');
        }
        //leads = leads.split(',');
        leads.forEach(function(item,index){
            if(item ==="") leads.splice(index,1);
        });
        return leads;
    };
</script>
</body>
</html>