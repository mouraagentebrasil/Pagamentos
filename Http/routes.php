<?php

Route::group(['middleware' => ['web','cors'], 'prefix' => 'pagamentos', 'namespace' => 'Modules\GatewayAb\Http\Controllers'], function()
{
    Route::get('/generateBoleto/{lead_id}/{vencimento}','GatewayAbController@generateBoleto');

    Route::get('/generateBoletoFinanceiroAB/{financeiro_cobranca_id}','GatewayAbController@generateBoletoNewModel');

    Route::get('/generateBoletoWithoutMup/{lead_id}/{vencimento}/{cobranca}','GatewayAbController@generateBoletoWithoutMup');

    Route::get('/cobranca_vencimento',function(){
        return view('gatewayab::cobranca_vencimento');
    });


    Route::get('/vb/{cobranca}','GatewayAbController@vb');

    Route::get('/getLeadVencimento/{data_inicial}/{data_final?}','GatewayAbController@listCobranca');

    Route::get('/generateBoletosVencimentos/{data_inicial}/{lead_id}','GatewayAbController@generateBoletosVencimentos');

    //
    Route::post('generateCobCardCredit','GatewayCieloController@generateCobCardCredit');
    //
    //Wrapper Cielo
    Route::get('wrapper-cielo/getSellCielo/{paymentId}','WrapperCielo@getSellCielo');

    Route::get('wrapper-cielo/putCancelSellCielo/{paymentId}','WrapperCielo@putCancelSellCielo');

    Route::post('wrapper-cielo/postSellCielo','WrapperCielo@postSellCielo');

    Route::get('wrapper-cielo/putCapureSellCielo/{paymentId}','WrapperCielo@putCapureSellCielo');
    //
});
