<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 10/01/2017
 * Time: 11:15
 */

namespace Modules\GatewayAb\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Routing\Controller;
use Modules\GatewayAb\Http\Controllers\WrapperCielo;

class GatewayCieloController extends Controller
{

    protected $card_status = [
        0=>'not_finish',
        1=>'authorized',
        2=>'payment_confirmed',
        3=>'denied',
        10=>'voided',
        11=>'refunded',
        12=>'pending',
        13=>'aborted'
    ];
    public function validate($data)
    {
        $message = [];

        if( !isset($data['Payment']['CreditCard']['CardNumber']) || is_null($data['Payment']['CreditCard']['CardNumber']) || empty($data['Payment']['CreditCard']['CardNumber']) )
            $message[] = 'Informar o numero do cartão de credito (Payment.CreditCard.CardNumber)';
        if( !isset($data['Payment']['CreditCard']['Holder']) || is_null($data['Payment']['CreditCard']['Holder']) || empty($data['Payment']['CreditCard']['Holder']) )
            $message[] = 'Informar o nome do titular do cartão (Payment.CreditCard.Holder)';
        if( !isset($data['Payment']['CreditCard']['ExpirationDate']) || is_null($data['Payment']['CreditCard']['ExpirationDate']) || empty($data['Payment']['CreditCard']['ExpirationDate']) )
            $message[] = 'Informar o vencimento do cartão (Payment.CreditCard.ExpirationDate)';
        if( !isset($data['Payment']['CreditCard']['SecurityCode']) || is_null($data['Payment']['CreditCard']['SecurityCode']) || empty($data['Payment']['CreditCard']['SecurityCode']) )
            $message[] = 'Informar o codigo de segurança (Payment.CreditCard.SecurityCode)';
        if( !isset($data['Payment']['CreditCard']['Brand']) || is_null($data['Payment']['CreditCard']['Brand']) || empty($data['Payment']['CreditCard']['Brand']) )
            $message[] = 'Informar a bandeira do cartão (Payment.CreditCard.Brand)';
        if( !isset($data['financeiro_cobranca_id']) || is_null($data['financeiro_cobranca_id']))
            $message[] = 'Informar a financeiro_cobranca_id';

        if(!empty($message)) return $message;
        return null;
    }

    public function generateCobCardCredit(Request $request)
    {
        try{

            $wrapperCielo = new WrapperCielo();
            $data = $request->all();
            $validate = $this->validate($data);

            if(!is_null($validate)) return response()->json(['message'=>$validate],400);

            $infoLead = $this->infoLead($data['financeiro_cobranca_id']);
            if(empty($infoLead)) return response()->json(['message'=>['cobranca não encontrada']],400);

            $data['Payment']['Type'] = 'CreditCard';
            $data['Payment']['Installments'] = 1;
            $data['Payment']['Amount'] = $infoLead->valor*100;
            $data['Customer']['Name'] = $infoLead->lead_nome;
            $data['MerchantOrderId'] = $data['financeiro_cobranca_id'];

            $sell = $wrapperCielo->postSellCielo($request,$data);
            if(gettype($sell)!=="string") return response()->json(['message'=>$sell->getData()],400);
            $sell = json_decode($sell);
            $paymentId = $sell->Payment->PaymentId;
            file_put_contents(storage_path("logs/log_cielo/postSell_".$paymentId.".txt"),json_encode($sell));
            $capture = $wrapperCielo->putCapureSellCielo($paymentId);
            file_put_contents(storage_path("logs/log_cielo/captureSell_".$paymentId.".txt"),json_encode($capture));
            //
            if(gettype($capture)!=="string") {
                $get_sell = $wrapperCielo->getSellCielo($paymentId);
                if(gettype($get_sell)!=="string") return response()->json(['message'=>$get_sell->getData()],400);
                $get_sell = json_decode($get_sell);
                file_put_contents(storage_path("logs/log_cielo/getSell_".$paymentId.".txt"),json_encode($get_sell));
                if($get_sell->Payment->Status==3){
                    $saveCobCielo = $this->saveCieloCobranca(
                        $infoLead,$paymentId,null, null,$this->card_status[$get_sell->Payment->Status]
                    );
                    return response()->json([
                        'paymentId'=>$paymentId,
                        'status'=>'denied',
                        'data_pagamento'=>null,
                        'valor_pago'=>null
                    ],200);
                }
                return response()->json(['message'=>$capture->getData()],400);
            }
            //
            $get_sell = $wrapperCielo->getSellCielo($paymentId);
            if(gettype($get_sell)!=="string") return response()->json(['message'=>$get_sell->getData()],400);
            $get_sell = json_decode($get_sell);
            file_put_contents(storage_path("logs/log_cielo/getSell_".$paymentId.".txt"),json_encode($get_sell));
            //
            if($get_sell->Payment->Status==2){

                $saveCobCielo = $this->saveCieloCobranca(
                    $infoLead,$paymentId,$get_sell->Payment->CapturedAmount,
                    $get_sell->Payment->CapturedDate,$this->card_status[$get_sell->Payment->Status]
                );
                return response()->json([
                    'paymentId'=>$paymentId,
                    'status'=>'pago',
                    'data_pagamento'=>$get_sell->Payment->CapturedDate,
                    'valor_pago'=>$get_sell->Payment->CapturedAmount/100
                ],200);
            }
            //
            if($get_sell->Payment->Status==12){
                for($i=0;$i<2;$i++){
                    sleep(4);
                    $getsell = $wrapperCielo->getSellCielo($paymentId);
                    $getsell = json_decode($getsell);
                    file_put_contents(storage_path("logs/log_cielo/getSell_".$paymentId."_".$i.".txt"),json_encode($getsell));
                    if(gettype($getsell)!=="string") return response()->json(['message'=>$getsell->getData()],400);

                    if($getsell->Payment->Status==2){
                        $this->saveCieloCobranca(
                            $infoLead,$paymentId,$getsell->Payment->CapturedAmount,
                            $getsell->Payment->CapturedDate,$this->card_status[$getsell->Payment->Status]
                        );
                        return response()->json([
                            'paymentId'=>$paymentId,
                            'status'=>'pago',
                            'data_pagamento'=>$get_sell->Payment->CapturedDate,
                            'valor_pago'=>$get_sell->Payment->CapturedAmount/100
                        ],200);
                    }
                }
            }
            //
            $getsell = $wrapperCielo->getSellCielo($paymentId);
            $getsell = json_decode($getsell);
            if(gettype($getsell)!=="string") return response()->json(['message'=>$getsell->getData()],400);
            file_put_contents(storage_path("logs/log_cielo/getSell_".$paymentId."_finally.txt"),json_encode($getsell));
            $this->saveCieloCobranca(
                    $infoLead,$paymentId,$getsell->Payment->CapturedAmount,
                    $getsell->Payment->CapturedDate,$this->card_status[$getsell->Payment->Status]
            );

            return response()->json([
                'paymentId'=>$paymentId,
                'status'=>$this->card_status[$getsell->Payment->Status],
                'data_pagamento'=>null,
                'valor_pago'=>null
            ],400);

        } catch (\Exception $e){
            return response()->json(['message'=>[$e->getMessage()]],400);
        }
    }

    public function saveCieloCobranca($infoLead, $paymentId,$valor_pago=null,$data_pagamento=null,$status)
    {
        switch ($status){
            case 'payment_confirmed':
                $status="pago";
                break;
            case 'voided':
                $status="cancelado";
                break;
            case 'denied':
            case 'aborted':
            case 'not_finish':
                $status="nao_autorizado";
                break;
            case 'pending':
                $status="pendente";
                break;
        }
        $instrucoes = $this->getInstrucoes($infoLead->lead_produto_agregados_id,$infoLead->produto_nome);
        try {
            DB::beginTransaction();
            $cobCon = DB::connection('mysql2')->table('cobranca')->insertGetId([
                'lead_id' => $infoLead->lead_id,
                'usuario_id' => $infoLead->usuario_id,
                'valor_pagamento' => $infoLead->valor,
                'gateway_pagamento' => 'CardCredit|Cielo',
                'produto_gateway_code' => $paymentId,
                'seguradora_id' => $infoLead->seguradora_id,
                'forma_pagamento' => 'cartao',
                'data_vencimento' => $infoLead->data_vencimento,
                'valor_pago' => ($valor_pago/100),
                'data_pagamento' => $data_pagamento,
                'data_inclusao' => date('Y-m-d H:i:s'),
                'status' => $status,
                'sacado_cpf' => $infoLead->lead_cpf,
                'financeiro_cobranca_id' => $infoLead->financeiro_cobranca_id,
                'instrucoes' => $instrucoes
            ]);
            DB::connection('mysql3')->table('financeiro_cobranca')
                ->where('financeiro_cobranca_id', $infoLead->financeiro_cobranca_id)
                ->update([
                    'gateway_slug' => 'CardCredit|Cielo',
                    'foi_pago' =>$status =="pago" ? 1 : 0,
                    'valor_pago' => !is_null($valor_pago) ? ($valor_pago/100) :  null,
                    'data_pago' => !is_null($data_pagamento) ? $data_pagamento : null,
                    'gateway_current_code' => $cobCon,
                    'financeiro_cobranca_status'=>$status //mudar
                ]);
            DB::commit();
            return true;
        } catch (\Exception $e){
            DB::rollback();
            //var_dump($e->getMessage()); exit;
            return false;
        }
    }

    public function infoLead($financeiro_cobranca_id)
    {
        $infoLead = DB::connection('mysql3')->select(
        'select 
        lpa.lead_id, 
        fcob.data_vencimento, fcob.valor, fcob.usuario_id, fcob.financeiro_cobranca_id,
        p.seguradora_id, l.lead_cpf, p.produto_nome, l.lead_nome, fc.valor_mensal, lpa.lead_produto_agregados_id
        from lead_produto_agregado lpa 
        inner join lead l on l.lead_id = lpa.lead_id
        inner join produto p on lpa.produto_id = p.produto_id
        inner join financeiro_contratacao fc on fc.lead_produto_agregados_id = lpa.lead_produto_agregados_id
        inner join financeiro_cobranca fcob on fcob.financeiro_contratacao_id = fc.financeiro_contratacao_id
        where fcob.financeiro_cobranca_id = :id',['id'=>$financeiro_cobranca_id]);
        if(!isset($infoLead[0])) return [];
        return $infoLead[0];
    }

    public function getInstrucoes($lead_produto_agregados_id,$produto_nome){
        $leadProdutoAgregadoItens = DB::connection('mysql3')
            ->select(
                'select lpai.lead_agregado_id, la.lead_agregado_nome 
                 from lead_produto_agregado_itens lpai 
                 left join lead_agregado la on la.lead_agregado_id = lpai.lead_agregado_id
                 where lpai.lead_produto_agregados_id = :id',['id'=>$lead_produto_agregados_id]);
        if(empty($leadProdutoAgregadoItens)) return null;
        $instrucoes = "";
        for($i=0;$i<count($leadProdutoAgregadoItens);$i++){
            $virgulaFinal = $i==count($leadProdutoAgregadoItens)-1 ? "" : ",";
            $instrucoes .= "Ref: ".$leadProdutoAgregadoItens[$i]->lead_agregado_nome." - ".$produto_nome.$virgulaFinal;
        }
        return $instrucoes;
    }
}