<?php
/**
 * Created by PhpStorm.
 * User: felipemoura
 * Date: 09/01/2017
 * Time: 17:23
 */

namespace Modules\GatewayAb\Http\Controllers;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Routing\Controller;


//Classe de integração com a cielo
class WrapperCielo extends Controller
{
    public $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    /***
     * @param $paymentId
     * @return object json
     * @desc retorna um objeto com informações referente ao pagamento passado
     */
    public function getSellCielo($paymentId)
    {
        try {
            $request = $this->client->request('GET',getenv('API_CIELO_CONS').'/1/sales/'.$paymentId,[
                'headers'=>[
                    'MerchantId'=>getenv('API_CIELO_MERCHANTID'),
                    'MerchantKey'=>getenv('API_CIELO_MERCHANTKEY'),
                    'RequestId'=>'GET',
                    'Content-Type'=>'application/json'
                ]
            ]);
            $content = $request->getBody()->getContents();
            return $content;
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }

    /**
     * @param Request $request dados enviados
     * @return object json
     * @desc envia uma compra para o gateway de pagamento, dados para envio podem ser encontrados aqui,
     * https://developercielo.github.io/Webservice-3.0/?json#criando-uma-transação-simples
     */
    public function postSellCielo(Request $request = null, $rdata = null)
    {

        try {
            $data = (!is_null($rdata)) ? $rdata : $request->all();
            $request = $this->client->request('POST',getenv('API_CIELO_TRANS')."/1/sales",[
                'headers'=>[
                    'MerchantId'=>getenv('API_CIELO_MERCHANTID'),
                    'MerchantKey'=>getenv('API_CIELO_MERCHANTKEY'),
                    'RequestId'=>'POST',
                    'Content-Type'=>'application/json'
                ],
                'json'=>$data
            ]);
            return $request->getBody()->getContents();
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }

    /***
     * @param $paymentId
     * @desc é preciso capturar uma venda passando um $paymentId
     * @return object json
     */
    public function putCapureSellCielo($paymentId)
    {
        try {
            $request = $this->client->request('PUT',getenv('API_CIELO_TRANS').'/1/sales/'.$paymentId."/capture",[
                'headers'=>[
                    'MerchantId'=>getenv('API_CIELO_MERCHANTID'),
                    'MerchantKey'=>getenv('API_CIELO_MERCHANTKEY'),
                    'RequestId'=>'PUT',
                    'Content-Type'=>'application/json'
                ]
            ]);
            $content = $request->getBody()->getContents();
            return $content;
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }

    /***
     * @param $paymentId
     * @return \Illuminate\Http\JsonResponse|string
     * @desc Cancela uma venda cielo
     */
    public function putCancelSellCielo($paymentId)
    {
        try {
            $request = $this->client->request('PUT',getenv('API_CIELO_TRANS').'/1/sales/'.$paymentId."/void",[
                'headers'=>[
                    'MerchantId'=>getenv('API_CIELO_MERCHANTID'),
                    'MerchantKey'=>getenv('API_CIELO_MERCHANTKEY'),
                    'RequestId'=>'PUT',
                    'Content-Type'=>'application/json'
                ]
            ]);
            $content = $request->getBody()->getContents();
            return $content;
        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage()],400);
        }
    }
}