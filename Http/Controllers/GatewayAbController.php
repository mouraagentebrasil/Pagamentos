<?php

namespace Modules\GatewayAb\Http\Controllers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Symfony\Component\DomCrawler\Crawler;
use Illuminate\Routing\Controller;

class GatewayAbController extends Controller
{
    public $client;
    public function __construct()
    {
        $this->client = new Client();
    }

    public function generateBoleto($lead_id,$vencimento)
    {
        set_time_limit(0);
        shell_exec('free && sync && echo 3 > /proc/sys/vm/drop_caches && free');
        clearstatcache();
        try {
            //$infoLead = DB::connection('mysql3')->table('cobranca')
            $r = $this->client->request('GET','http://agentebrasil.com/sys-v2/gatewayAb/getInfoLeadPlan/'.$lead_id);
            $infoLead = json_decode($r->getBody()->getContents());
            $data = [
                'lead_id'=>$lead_id,
                'vencimento'=>$vencimento,
                'action'=>'gerarBoletoPelaAuditoria'
            ];

            $r = $this->client->requestAsync('POST', 'http://agentebrasil.com/fab/router.php', [
                'form_params' => $data
            ]);
            $respoP = $r->wait();
            $response = $respoP->getBody()->getContents();
            $part = explode ('!@#',$response);
            if($part[0]==1){
                $cobranca = $part[1];
                $r = $this->client->requestAsync('GET', 'http://mup.comercioeletronico.com.br/paymethods/conteudo/pagamento/frame_boleto_retNOVO.asp?Merchantid=100005559&orderid='.$cobranca.'&');
                $objRet = $r->wait();
                $objRet = $objRet->getBody()->getContents();
                //var_dump($objRet); exit;
                //GZUIS
                $objRet = explode("<script language='javascript'>    var url;    var parametro;    parametro = '&comB=",$objRet);
                $objRet = explode("';    url = 'mup.comercioeletronico.com.br/paymethods/conteudo/pagamento/frame_boleto_retNOVO.asp?Merchantid=100005559&orderid=".$cobranca."';    window.top.location.href = 'http://' + url + parametro;",$objRet[1]);

                $comb = $objRet[0];
                //var_dump($cobranca); var_dump($infoLead); var_dump($comb); exit;
                $r = $this->client->requestAsync('GET','http://mup.comercioeletronico.com.br/paymethods/conteudo/pagamento/frame_boleto_retNOVO.asp?Merchantid=100005559&orderid='.$cobranca.'&comB='.$comb);
                $response = $r->wait();
                //file_put_contents(public_path('html_boletos/'.$cobranca.'_original.html'),$r);

                $output = (shell_exec('wkhtmltoimage --crop-h 612 --crop-w 776 --crop-x 123 --crop-y 766 --quality 89 http://agentebrasil.com/fab/boletos/'.$cobranca.'.html '.public_path('boletos/').$cobranca.'.png'));
                $boleto = file_get_contents(storage_path('template/template_boleto.html'));
                $boleto = str_replace('[QTD]',$infoLead->lead->lead_quantidade,$boleto);
                $boleto = str_replace('[PLANO]',$infoLead->plan->plan_descricao,$boleto);
                $valor = $infoLead->lead->lead_quantidade * $infoLead->plan->plan_valor;
                $boleto = str_replace('[VENC]',$vencimento,$boleto);
                $boleto = str_replace('[VALOR_TOTAL]',$valor,$boleto);
                $boleto = str_replace('[LEAD_CPF]',$infoLead->lead->lead_cpf,$boleto);
                $boleto = str_replace('[LEAD_NOME]',strtoupper($infoLead->lead->lead_nome),$boleto);
                $boleto = str_replace('[IMG]','http://pag.agentebrasil.com/boletos/'.$cobranca.'.png',$boleto);

                file_put_contents(public_path('html_boletos/'.$cobranca."_final.html"),$boleto);
                $output = (shell_exec('wkhtmltoimage  --crop-h 1050 --crop-w 774 --crop-x 9 --crop-y 0 --quality 89  http://pag.agentebrasil.com/html_boletos/'.$cobranca.'_final.html '.public_path('boletos/').$cobranca.'_f.png'));
                $r = $this->client->request('GET','http://agentebrasil.com/sys-v2/gatewayAb/getLinhaDigitavel/'.$cobranca);
                $linha = json_decode($r->getBody()->getContents());
                clearstatcache();
                return response()->json([
                    'cobranca'=>$cobranca,
                    'file'=>'http://pag.agentebrasil.com/gatewayab/vb/'.$cobranca,
                    'linha_digitavel'=>$linha->linha_digitavel,
                    'valor_fatura'=>$linha->valor,
                    'vencimento'=>$linha->data_vencimento
                ]);
            } else {
                clearstatcache();
                return response()->json(['message'=>'Erro'],400);
            }
        } catch (\Exception $e){
            clearstatcache();
            return response()->json(['message'=>$e->getMessage().'\n'.$e->getFile().'\n'.$e->getLine()],400);
        }
    }

    function tirarAcentos($string){
        return preg_replace(array("/(á|à|ã|â|ä)/","/(Á|À|Ã|Â|Ä)/","/(é|è|ê|ë)/","/(É|È|Ê|Ë)/","/(í|ì|î|ï)/","/(Í|Ì|Î|Ï)/","/(ó|ò|õ|ô|ö)/","/(Ó|Ò|Õ|Ô|Ö)/","/(ú|ù|û|ü)/","/(Ú|Ù|Û|Ü)/","/(ñ)/","/(Ñ)/"),explode(" ","a A e E i I o O u U n N"),$string);
    }

    public function infoLead($financeiro_cobranca_id)
    {
        $infoLead = DB::connection('mysql3')->select(
            'select 
        lpa.lead_id, 
        fcob.data_vencimento, fcob.valor, fcob.usuario_id, fcob.financeiro_cobranca_id,
        p.seguradora_id, l.lead_cpf, p.produto_nome, l.lead_nome, fc.valor_mensal, lpa.lead_produto_agregados_id
        from lead_produto_agregado lpa 
        inner join lead l on l.lead_id = lpa.lead_id
        inner join produto p on lpa.produto_id = p.produto_id
        inner join financeiro_contratacao fc on fc.lead_produto_agregados_id = lpa.lead_produto_agregados_id
        inner join financeiro_cobranca fcob on fcob.financeiro_contratacao_id = fc.financeiro_contratacao_id
        where fcob.financeiro_cobranca_id = :id',['id'=>$financeiro_cobranca_id]);
        if(!isset($infoLead[0])) return [];
        return $infoLead[0];
    }

    public function getInstrucoes($lead_produto_agregados_id,$produto_nome){
        $leadProdutoAgregadoItens = DB::connection('mysql3')
            ->select(
                'select lpai.lead_agregado_id, la.lead_agregado_nome 
                 from lead_produto_agregado_itens lpai 
                 left join lead_agregado la on la.lead_agregado_id = lpai.lead_agregado_id
                 where lpai.lead_produto_agregados_id = :id',['id'=>$lead_produto_agregados_id]);
        //var_dump($leadProdutoAgregadoItens); exit;
        if(empty($leadProdutoAgregadoItens)) return null;
        //var_dump($leadProdutoAgregadoItens); exit;
        $instrucoes = "";
        for($i=0;$i<count($leadProdutoAgregadoItens);$i++){
            $virgulaFinal = $i==count($leadProdutoAgregadoItens)-1 ? "" : ",";
            $instrucoes .= "Ref: ".$leadProdutoAgregadoItens[$i]->lead_agregado_nome." - ".$produto_nome.$virgulaFinal;
        }
        //var_dump($instrucoes); exit;
        return $instrucoes;
    }

    public function getQtdProduto($lead_produto_agregados_id){
        $leadProdutoAgregadoItens = DB::connection('mysql3')
            ->select(
                'select count(*) as total from lead_produto_agregado_itens lpai 
                 inner join lead_agregado la on la.lead_agregado_id = lpai.lead_agregado_id
                 where lpai.lead_produto_agregados_id = :id',['id'=>$lead_produto_agregados_id]);
        //var_dump($leadProdutoAgregadoItens); exit;
        if(empty($leadProdutoAgregadoItens)) return null;
        return $leadProdutoAgregadoItens[0]->total;
    }

    public function generateBoletoNewModel($financeiro_cobranca_id)
    {
        set_time_limit(0);
        shell_exec('free && sync && echo 3 > /proc/sys/vm/drop_caches && free');
        clearstatcache();
        try {
            $infoLead = $this->infoLead($financeiro_cobranca_id);
            $data = [
                'financeiro_cobranca_id'=>$financeiro_cobranca_id,
                //'vencimento'=>$vencimento,
                'action'=>'gerarBoletoByCobABStart',
                'instrucoes'=>$this->getInstrucoes($infoLead->lead_produto_agregados_id,$infoLead->produto_nome)
            ];

            $r = $this->client->requestAsync('POST', 'http://agentebrasil.com/fab/router.php', [
                'form_params' => $data
            ]);
            $respoP = $r->wait();
            $response = $respoP->getBody()->getContents();
            $part = explode ('!@#',$response);
            if($part[0]==1){
                $cobranca = $part[1];
                $r = $this->client->requestAsync('GET', 'http://mup.comercioeletronico.com.br/paymethods/conteudo/pagamento/frame_boleto_retNOVO.asp?Merchantid=100005559&orderid='.$cobranca.'&');
                $objRet = $r->wait();
                $objRet = $objRet->getBody()->getContents();
                //GZUIS
                $objRet = explode("<script language='javascript'>    var url;    var parametro;    parametro = '&comB=",$objRet);
                $objRet = explode("';    url = 'mup.comercioeletronico.com.br/paymethods/conteudo/pagamento/frame_boleto_retNOVO.asp?Merchantid=100005559&orderid=".$cobranca."';    window.top.location.href = 'http://' + url + parametro;",$objRet[1]);

                $comb = $objRet[0];
                $r = $this->client->requestAsync('GET','http://mup.comercioeletronico.com.br/paymethods/conteudo/pagamento/frame_boleto_retNOVO.asp?Merchantid=100005559&orderid='.$cobranca.'&comB='.$comb,['timeout' => 50]);
                $response = $r->wait();

                $output = (shell_exec('wkhtmltoimage --crop-h 612 --crop-w 776 --crop-x 123 --crop-y 766 --quality 89 http://agentebrasil.com/fab/boletos/'.$cobranca.'.html '.public_path('boletos/').$cobranca.'.png'));
                $boleto = file_get_contents(storage_path('template/template_boleto.html'));
                $boleto = str_replace('[QTD]',$this->getQtdProduto($infoLead->lead_produto_agregados_id),$boleto);
                $boleto = str_replace('[PLANO]',$infoLead->produto_nome,$boleto);
                $valor = $infoLead->valor;
                $boleto = str_replace('[VENC]',$infoLead->data_vencimento,$boleto);
                $boleto = str_replace('[VALOR_TOTAL]',$valor,$boleto);
                $boleto = str_replace('[LEAD_CPF]',$infoLead->lead_cpf,$boleto);
                $boleto = str_replace('[LEAD_NOME]',strtoupper($infoLead->lead_nome),$boleto);
                $boleto = str_replace('[IMG]',getenv('SERVICO_PAGAMENTO').'/boletos/'.$cobranca.'.png',$boleto);
                //
                file_put_contents(public_path('html_boletos/'.$cobranca."_final.html"),$boleto);
                //
                $boleto2 = file_get_contents('http://agentebrasil.com/fab/boletos/'.$cobranca.'.html');
                $crawler = new Crawler($boleto2);
                $nosso_numero = $crawler->filter('.right.textRight')->eq(3)->text();
                //
                DB::connection('mysql2')->table('cobranca')->where('cobranca_id',$cobranca)->update([
                    'nosso_numero'=>$nosso_numero
                ]);
                $output = (shell_exec('wkhtmltoimage  --crop-h 1050 --crop-w 774 --crop-x 9 --crop-y 0 --quality 89  '.getenv('SERVICO_PAGAMENTO').'/html_boletos/'.$cobranca.'_final.html '.public_path('boletos/').$cobranca.'_f.png'));

                $getCob = DB::connection('mysql2')->table('cobranca')->where('cobranca_id',$cobranca)->first();
                clearstatcache();
                return response()->json([
                    'cobranca'=>$cobranca,
                    'file'=>getenv('SERVICO_PAGAMENTO').'/pagamentos/vb/'.$cobranca,
                    'linha_digitavel'=>$getCob->linha_digitavel,
                    'valor_fatura'=>$getCob->valor_pagamento,
                    'vencimento'=>$getCob->data_vencimento
                ]);
            } else {
                clearstatcache();
                return response()->json(['message'=>'Erro'],400);
            }
        } catch (\Exception $e){
            clearstatcache();
            return response()->json(['message'=>$e->getMessage().'\n'.$e->getFile().'\n'.$e->getLine()],400);
        }
    }

    public function generateBoletoWithoutMup($lead_id,$vencimento,$cobranca)
    {
        set_time_limit(0);
        try {
            $r = $this->client->request('GET','http://agentebrasil.com/sys-v2/gatewayAb/getInfoLeadPlan/'.$lead_id);
            $infoLead = json_decode($r->getBody()->getContents());
            $output = (shell_exec('wkhtmltoimage --crop-h 612 --crop-w 776 --crop-x 123 --crop-y 766 --quality 89 http://agentebrasil.com/fab/boletos/'.$cobranca.'.html '.public_path('boletos/').$cobranca.'.png'));
            $boleto = file_get_contents(storage_path('template/template_boleto.html'));
            $boleto = str_replace('[QTD]',$infoLead->lead->lead_quantidade,$boleto);
            $boleto = str_replace('[PLANO]',$infoLead->plan->plan_descricao,$boleto);
            $valor = $infoLead->lead->lead_quantidade * $infoLead->plan->plan_valor;
            $boleto = str_replace('[VENC]',$vencimento,$boleto);
            $boleto = str_replace('[VALOR_TOTAL]',$valor,$boleto);
            $boleto = str_replace('[LEAD_CPF]',$infoLead->lead->lead_cpf,$boleto);
            $boleto = str_replace('[LEAD_NOME]',strtoupper($infoLead->lead->lead_nome),$boleto);
            $boleto = str_replace('[IMG]','http://pag.agentebrasil.com/boletos/'.$cobranca.'.png',$boleto);

            file_put_contents(public_path('html_boletos/'.$cobranca."_final.html"),$boleto);
            $output = (shell_exec('wkhtmltoimage  --crop-h 1050 --crop-w 776 --crop-x 9 --crop-y 0 --quality 89  http://pag.agentebrasil.com/html_boletos/'.$cobranca.'_final.html '.public_path('boletos/').$cobranca.'_f.png'));
            $r = $this->client->request('GET','http://agentebrasil.com/sys-v2/gatewayAb/getLinhaDigitavel/'.$cobranca);
            $linha = json_decode($r->getBody()->getContents());
            return response()->json([
                'cobranca'=>$cobranca,
                'file'=>'https://pag.agentebrasil.com/gatewayab/vb/'.$cobranca,
                'linha_digitavel'=>$linha->linha_digitavel,
                'valor_fatura'=>$linha->valor,
                'vencimento'=>$linha->data_vencimento
            ]);

        } catch (\Exception $e){
            return response()->json(['message'=>$e->getMessage().'\n'.$e->getFile().'\n'.$e->getLine()],400);
        }
    }

    public function listCobranca($data_inicial, $data_final = null)
    {
        $leadF = DB::connection('mysql')->table('leads');
        $leadF->where('lead_vencimento_dia','>=',$data_inicial);
        if(!is_null($data_final)) {
            $leadF->where('lead_vencimento_dia','<=',$data_final);
        } else {
            $leadF->where('lead_vencimento_dia','<=',$data_inicial);
        }
        return $leadF->get();
    }

    public function generateBoletosVencimentos($vencimento,$lead_id)
    {
        set_time_limit(0);
        $leadsDB = DB::connection('mysql')->table('cobrancas');
        $find = $leadsDB
            ->where('responsavel_financeiro',$lead_id)
            ->where('vencimento',$vencimento)
            ->whereNotIn('status',['Pago'])
            ->first();

        if(is_null($find)){
            //var_dump(getmygid()); exit;
            return $this->generateBoleto($lead_id,$vencimento);
        }
        if(file_exists(public_path('boletos/'.$find->cod.'_f.png'))){
            //var_dump(getmygid()); exit;
            return response()->json([
                'cobranca'=>$find->cod,
                'file'=>'https://pag.agentebrasil.com/pagamentos/vb/'.$find->cod,
                'linha_digitavel'=>$find->linha_digitavel,
                'valor_fatura'=>$find->valor,
                'vencimento'=>$find->vencimento
            ]);
        }

        return $this->generateBoletoWithoutMup($lead_id,$vencimento,$find->cod);
    }

    public function diffDates($date1,$date2)
    {
        $time_inicial = strtotime($date1);
        $time_final = strtotime($date2);
        $diferenca = $time_final - $time_inicial;
        $dias = (int)floor( $diferenca / (60 * 60 * 24));
        return $dias;
    }

    public function vb($cobranca){
        //$this->client->request('get',getenv('SERVICE_CONTACT').'/contact/setCob/'.$cobranca);
        return view('gatewayab::view_boletos',['cobranca'=>$cobranca]);
    }

}