<?php

namespace Modules\GatewayAb\Console;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class CheckAtualizaCobCommand extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'update_cob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Atualiza cobrança';
    protected $client;
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $call = $this->client->request('GET','http://agentebrasil.com/fab/robo-bradesco-verifica-pagamento.php');
        var_dump($call->getBody()->getContents());
        var_dump($call->getStatusCode());
    }

}
