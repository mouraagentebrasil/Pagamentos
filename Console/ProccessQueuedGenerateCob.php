<?php

namespace Modules\GatewayAb\Console;

use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Modules\GatewayAb\Http\Controllers\GatewayAbController;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Illuminate\Http\JsonResponse;
use Modules\GatewayAb\Entities\LogsGeracaoCobranca as Logs;

class ProccessQueuedGenerateCob extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'proccess_queued_cob';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Processa boletos';
    protected $client;
    protected $meses = [
        1 => 'Janeiro',
        'Fevereiro',
        'Março',
        'Abril',
        'Maio',
        'Junho',
        'Julho',
        'Agosto',
        'Setembro',
        'Outubro',
        'Novembro',
        'Dezembro'
    ];
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->client = new Client();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        //
        $log_cob = time().'_'.date('Y-m-d').'.txt';
        #
        $today = date('Y-m-d');
        #$today = '2016-12-02';
        $lastSevenDays = date('Y-m-d',strtotime('-25 days',strtotime($today)));
        var_dump($today);
        var_dump($lastSevenDays);
        $leads = DB::connection('mysql')->table('leads')
            ->select([
                'lead_id','lead_cpf','lead_vencimento_dia','lead_email',
                'lead_quantidade','plan_id','lead_nome','lead_fone'
            ])
            ->where('lead_vencimento_dia','>=',$lastSevenDays)
            ->where('lead_vencimento_dia','<=',$today)
            ->where('lead_id','121268')
            ->get();
        //var_dump($leads); exit;
        $msg = "";
        $msg .= "Cobrança Range ".$lastSevenDays." até ".$today.PHP_EOL.'Numero de leads encontrados: '.count($leads).PHP_EOL;
        foreach($leads as $lead){
            $msg .= 'Inciando cobranca de '.$lead->lead_id.' - '.$lead->lead_nome.PHP_EOL;
            $cobranca = DB::connection('mysql')->table('cobrancas')
                ->where('responsavel_financeiro',$lead->lead_id)
                ->where('vencimento','>=',$lastSevenDays)
                ->where('vencimento','<=',$today)
                ->first();

            $date_initial = date('Y-m-d H:i:s');
            $cobranca_gerada = 0;
            $send_email = 0;
            $send_sms = 0;
            $cobranca_id = 0;
            $arquivo_cobranca = null;
            $msg .= 'Tentando gerar cobranca para o lead_id: '.$lead->lead_id.'...'.PHP_EOL;
            if(is_null($cobranca)){
                $call = $this->callCobranca($lead->lead_id,$lead->lead_vencimento_dia);
                if(!is_object($call)) {
                    $msg .= 'Não foi possivel gerar a cobra... Erro: '.$cobranca.PHP_EOL;
                    $msg.= '-----------------------------------------------------'.PHP_EOL;
                    file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                    Logs::create([
                        'data_processamento_inicial'=>$date_initial,
                        'data_processamento_final'=>date('Y-m-d H:i:s'),
                        'cobranca_gerada'=>$cobranca_gerada,
                        'send_sms'=>$send_sms,
                        'send_email'=>$send_email,
                        'retorno_cobranca'=>json_encode($call)
                    ]);
                    continue;
                } else {
                    $msg .= 'Cobrança gerada com sucesso'.PHP_EOL.'Tentando Enviar SMS'.PHP_EOL;
                    //var_dump($call); exit;
                    $cobranca_id = $call->cobranca;
                    $arquivo_cobranca = $call->file;
                    $cobranca_gerada = 1;
                }
                $sms = $this->sendSms($lead,$call);
                if(!is_object($sms)){
                    $msg .= 'Não foi possivel enviar o sms... Erro: '.$sms.PHP_EOL;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                } else {
                    $msg .= 'SMS adicionado a Fila'.PHP_EOL;
                    $send_sms = 1;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                }
                $email = $this->sendEmail($lead,$call);
                if(!is_object($email)){
                    $msg .= 'Não foi possivel enviar o email... Erro: '.$email.PHP_EOL;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                } else {
                    $msg .= 'Email adicionado a Fila'.PHP_EOL;
                    $send_email = 1;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                }

            } else {
                $call = $this->callCobrancaExiste($lead->lead_id,$lead->lead_vencimento_dia,$cobranca->cod);
                $cobranca_id = $cobranca->cod;
                if(!is_object($call)) {
                    $msg .= 'Não foi possivel gerar a cobra... Erro: '.$cobranca.PHP_EOL;
                    $msg.= '-----------------------------------------------------'.PHP_EOL;
                    file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                    Logs::create([
                        'data_processamento_inicial'=>$date_initial,
                        'data_processamento_final'=>date('Y-m-d H:i:s'),
                        'cobranca'=>$cobranca->cod,
                        'cobranca_gerada'=>0,
                        'send_sms'=>0,
                        'send_email'=>0,
                        'retorno_cobranca'=>json_encode($call)
                    ]);
                    continue;
                } else {
                    $msg .= 'Cobrança gerada com sucesso'.PHP_EOL.'Tentando Enviar SMS'.PHP_EOL;
                    $cobranca_gerada = 1;
                    $arquivo_cobranca = $call->file;
                }
                $sms = $this->sendSms($lead,$call);
                if(!is_object($sms)){
                    $msg .= 'Não foi possivel enviar o sms... Erro: '.$sms.PHP_EOL;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                } else {
                    $msg .= 'SMS adicionado a Fila'.PHP_EOL;
                    $send_sms = 1;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                }
                $email = $this->sendEmail($lead,$call);
                if(!is_object($email)){
                    $msg .= 'Não foi possivel enviar o email... Erro: '.$email.PHP_EOL;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                } else {
                    $msg .= 'Email adicionado a Fila'.PHP_EOL;
                    $send_email = 1;
                    //file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
                }
            }

            $msg.= '-----------------------------------------------------'.PHP_EOL;
            file_put_contents(storage_path('logs/log_cob/').$log_cob,$msg,FILE_APPEND);
            Logs::create([
                'data_processamento_inicial'=>$date_initial,
                'data_processamento_final'=>date('Y-m-d H:i:s'),
                'cobranca'=>$cobranca_id,
                'arquivo_cobranca'=>$arquivo_cobranca,
                'cobranca_gerada'=>$cobranca_gerada,
                'send_sms'=>$send_sms,
                'send_email'=>$send_email,
                'retorno_sms'=>json_encode($sms),
                'retorno_email'=>json_encode($email),
                'retorno_cobranca'=>json_encode($call)
            ]);
        }
        return 0;
    }

    public function callCobrancaExiste($lead_id,$vencimento,$cobranca)
    {
        try {
            $call = (new GatewayAbController)->generateBoletoWithoutMup($lead_id,$vencimento,$cobranca);
            return $call->getData();
        } catch (\Exception $e){
            return $e->getFile().PHP_EOL.$e->getCode().PHP_EOL.$e->getLine();
        }
    }

    public function callCobranca($lead_id,$vencimento)
    {
        try {
            $call = $this->client->request(
                'GET',
                'pag.agentebrasil.com/gatewayab/generateBoleto/'.$lead_id.'/'.$vencimento
            );
            $response = json_decode($call->getBody()->getContents());
            return $response;
        } catch (\Exception $e){
            return $e->getMessage().PHP_EOL.$e->getFile().PHP_EOL.$e->getCode();
        }
    }

    public function sendSms($lead,$cobranca)
    {
        $humanizeDate = function ($date){
            $part = explode('-',$date);
            return $part[2]."/".$part[1];
        };
        var_dump($lead);
        try {
            $call = $this->client->request(
                'POST', 'contact.agentebrasil.com/contact/contact/store', [
                    'form_params'=>[
                        'lead_id'=>$lead->lead_id,
                        'canal_id'=>2,
                        'destinatario_fone'=>'55'.$lead->lead_fone,
                        'destinatario_nome'=>$lead->lead_nome,
                        'produto_nome_email'=>'Unimed Odonto',
                        'remetente_nome'=>'Agente Brasil',
                        'mensagem'=>'Segue sua fatura com o venc. em '.$humanizeDate($lead->lead_vencimento_dia).' http://pag.agentebrasil.com/boletos/'.$cobranca->cobranca.'_f.png ou '.$cobranca->linha_digitavel
                    ]
                ]
            );
            return json_decode($call->getBody()->getContents());
        } catch (\Exception $e){
            return $e->getMessage().'\n'.$e->getFile().'\n'.$e->getCode();
        }
    }

    public function sendEmail($lead,$cobranca)
    {
        $humanizeDate = function ($date){
            $part = explode('-',$date);
            return $part[2]."/".$part[1];
        };
        //var_dump($lead);
        try {
            $call = $this->client->request(
                'POST', 'contact.agentebrasil.com/contact/contact/store', [
                    'form_params'=>[
                        'lead_id'=>$lead->lead_id,
                        'canal_id'=>1,
                        'destinatario_fone'=>'55'.$lead->lead_fone,
                        'destinatario_email'=>$lead->lead_email,
                        'destinatario_nome'=>$lead->lead_nome,
                        'produto_nome_email'=>'Unimed Odonto',
                        'remetente_email'=>'noreply@agentebrasil.com',
                        'remetente_nome'=>'Agente Brasil',
                        'mensagem'=>'Olá '.$lead->lead_nome.', sua fatura com o vencimento em '.$humanizeDate($lead->lead_vencimento_dia).' já está disponivel para pagamento, acesse pelo link http://pag.agentebrasil.com/boletos/'.$cobranca->cobranca.'_f.png, ou use a linha digitavel de seu boleto '.$cobranca->linha_digitavel
                    ]
                ]
            );
            return json_decode($call->getBody()->getContents());
        } catch (\Exception $e){
            return $e->getMessage().'\n'.$e->getFile().'\n'.$e->getCode();
        }
    }
}
